/*
** my_redirections.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Tue May 12 10:36:36 2015 grandr_r
** Last update Sat May 23 18:47:37 2015 grandr_r
*/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <my.h>

int	p_redir_sright(char *s, t_sh *sh, char **tmp)
{
  char	**tmp2;
  int	fd;
  int	oldfd;

  oldfd = dup(1);
  if (tmp[1] == NULL)
    return (return_error("can't specify > as last word in a command\n", tmp));
  tmp2 = my_str_to_wordtab(tmp[1], 0, ' ');
  if ((fd = open(tmp2[0], O_CREAT | O_WRONLY | O_TRUNC, 0666)) == -1)
    return (function_error2("open : ", tmp2[0], tmp, tmp2));
  if (dup2(fd, 1) == -1)
    return (function_error("dup2: ", "error"));
  close(fd);
  my_logical_pipe_rec(sh, my_strdup(tmp[0]));
  if (dup2(oldfd, 1) == -1)
    return (function_error("dup2: ", "error"));
  my_free_wordtab(tmp);
  my_free_wordtab(tmp2);
  return (1);
}

int	p_redir_dright(char *s, t_sh *sh, char **tmp)
{
  char	**tmp2;
  int	fd;
  int	oldfd;

  oldfd = dup(1);
  if (tmp[1] == NULL)
    return (return_error("can't specify > as last word in a command\n", tmp));
  tmp2 = my_str_to_wordtab(tmp[1], 0, ' ');
  if ((fd = open(tmp2[0], O_CREAT | O_WRONLY | O_APPEND, 0666)) == -1)
    return (function_error2("open : ", tmp2[0], tmp, tmp2));
  if (dup2(fd, 1) == -1)
    return (function_error("dup2: ", "error"));
  close(fd);
  my_logical_pipe_rec(sh, my_strdup(tmp[0]));
  if (dup2(oldfd, 1) == -1)
    return (function_error("dup2: ", "error"));
  my_free_wordtab(tmp);
  my_free_wordtab(tmp2);
  return (1);
}

int	p_redir_sleft(char *s, t_sh *sh, char **tmp)
{
  char  **tmp2;
  int   fd;
  int	oldfd;

  oldfd = dup(0);
  if (tmp[1] == NULL)
    return (return_error("can't specify < as last word in a command\n", tmp));
  if ((tmp2 = malloc(sizeof(char *) * (my_arrlen(tmp) + 1))) == NULL)
    exit(-1);
  tmp2 = my_str_to_wordtab(tmp[1], 0, ' ');
  if ((fd = open(tmp2[0], O_RDONLY)) == -1)
    return (function_error2("open : ", tmp2[0], tmp, tmp2));
  if (dup2(fd, 0) == -1)
    return (function_error("dup2: ", "error"));
  close(fd);
  if (dup2(oldfd, 0) == -1)
    return (function_error("dup2: ", "error"));
  my_logical_pipe_rec(sh, my_strdup(tmp[0]));
  my_free_wordtab(tmp);
  my_free_wordtab(tmp2);
  return (1);
}

int	p_redir_dleft(char *s, t_sh *sh, char **tmp)
{
  char  *s2;
  char  **tmp2;
  char  *s3;
  int   fd;

  if (tmp[1] == NULL)
    return (return_error("can't specify < as last word in a command\n", tmp));
  tmp2 = my_str_to_wordtab(tmp[1], 0, ' ');
  if ((fd = open("/tmp/.redir", O_RDWR | O_CREAT | O_TRUNC, 0666)) == -1)
    return (function_error2("open : ", "/tmp/.redir", tmp, tmp2));
  while (write(1, "> ", 2) && (s2 = get_line(0)))
    {
      if (match(tmp2[0], s2) == 1)
        break;
      write(fd, s2, my_strlen(s2));
      write(fd, "\n", 1);
      free(s2);
    }
  s3 = my_strdup(tmp[0]);
  s3 = my_realloc(s3, " < /tmp/.redir");
  p_redir_sleft(s, sh, my_str_to_wordtab(s3, 0, '<'));
  free(s2);
  free(s3);
  my_free_wordtab3(tmp, tmp2);
  return (1);
}

int	pipe_is_redir(char *s, t_sh *sh)
{
  if (match(s, "*<<*") == 1)
    return (p_redir_dleft(s, sh, my_str_to_wordtab(s, 0, '<')));
  else if (match(s, "*<*") == 1)
    return (p_redir_sleft(s, sh, my_str_to_wordtab(s, 0, '<')));
  else if (match(s, "*>>*") == 1)
    return (p_redir_dright(s, sh, my_str_to_wordtab(s, 0, '>')));
  else if (match(s, "*>*") == 1)
    return (p_redir_sright(s, sh, my_str_to_wordtab(s, 0, '>')));
  return (0);
}
