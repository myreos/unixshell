/*
** main.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh/src
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Mar 10 15:05:16 2015 remy grandry
** Last update Wed Sep 25 14:13:54 2019 grandr_r
*/

#include <my.h>
#include <stdlib.h>
#include <stdio.h>

void	my_exec(t_42 **cmd, t_sh *sh, char *str)
{
  int	i;
  char	*s;

  i = 0;
  s = epur_str(str, ';');
  free(str);
  if (only_chars(s, ' ', ';', sh) == 1 && s != NULL)
    {
      sh->all_cmd = my_str_to_wordtab(s, 0, ';');
      free(s);
      my_uptdate_path(sh);
      while (sh->all_cmd[i])
	{
	  free(sh->epur_cmd[i]);
	  sh->epur_cmd[i] = epur_str(sh->all_cmd[i], ' ');
	  canon_off(sh);
	  my_shell_logical(cmd, sh, sh->epur_cmd[i]);
	  i++;
	  canon_on(sh);
	}
      my_free_wordtab(sh->all_cmd);
      my_free_wordtab(sh->epur_cmd);
    }
  else
    free(s);
}

void	init_42(int ac, char **av, char **env, t_sh *sh)
{
  int	i;

  i = 1;
  my_env_copy(sh, env);
  sh->safe = 0;
  sh->termcap = 1;
  while (i < ac)
    {
      if (match(av[i], "--termcap=on") == 1)
	sh->termcap = 1;
      else if (match(av[i], "--safe-mode") == 1)
	sh->safe = 1;
      i++;
    }
}

int	main(int ac, char **av, char **env)
{
  t_sh	*sh;
  t_42	*cmd;
  char	*str;

  if ((sh = malloc(sizeof(*sh))) == NULL)
    return (-1);
  init_42(ac, av, env, sh);
  cmd = init_list();
  init_all(cmd, sh, env);
  add_next(cmd, my_strdup(""));
  cmd = cmd->next;
  while (42)
    {
      disp_prompt(sh);
      if ((str = get_next_line(0, cmd, sh)) == NULL)
	return (-1);
      if (str[0] != '\0' && my_strcmp(str, cmd->prev->buff)
	  != 0 && match(str, "*!*") == 0)
	add_prev(cmd, epur_str(str, ' '));
      if (str[0] != '\0' && (str = convert_cmd(str, sh, 0, 0)))
	my_exec(&cmd, sh, str);
    }
  return (0);
}
