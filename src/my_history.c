/*
** my_history.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Wed May 13 19:13:07 2015 grandr_r
** Last update Sun May 24 13:05:42 2015 grandr_r
*/

#include <stdlib.h>
#include <stdio.h>
#include <my.h>

char	*disp_prev_cmd(char *s, t_42 *cmd, int ncmd, t_sh *sh)
{
  int	i;
  t_42 *root;

  i = 1;
  root = cmd->next;
  while (i <= ncmd && root != cmd)
    {
      if (i == ncmd)
	return (root->buff);
      root = root->next;
      i++;
    }
  my_putstr(s, 2);
  my_putstr(" :no such event\n", 2);
  return (NULL);
}

int	hist_mark(char *str, t_sh *sh, t_42 **cmd)
{
  char	*s;

  s = NULL;
  if (match(str, "!") == 1)
    return (1);
  if (match(str, "!!") == 1)
    s = my_strdup(cmd[0]->prev->buff);
  else
    s = my_strdup(disp_prev_cmd(str, cmd[0], my_getnbr(&str[1]) + 1, sh));
  if (s != NULL)
    my_shell_logical(cmd, sh, s);
  free(s);
  return (1);
}

void	new_hist(t_42 **cmd)
{
  t_42	*root;

  root = cmd[0]->prev;
  while (root != cmd[0])
    {
      root = root->prev;
      remove_elem(root->next);
    }
  remove_elem(root);
  cmd[0] = NULL;
  cmd[0] = init_list();
  add_next(cmd[0], my_strdup(""));
  cmd[0] = cmd[0]->next;
}

int	my_history(char *str, t_sh *sh, t_42 **cmd)
{
  char	**tmp;

  tmp = my_str_to_wordtab(str, 0, ' ');
  if (match(tmp[0], "history") == 0)
    {
      my_free_wordtab(tmp);
      return (0);
    }
  if (tmp[1] == NULL)
    {
      display_list(cmd[0]);
      my_free_wordtab(tmp);
      return (1);
    }
  if (match(tmp[1], "-c") == 1)
    {
      new_hist(cmd);
      my_free_wordtab(tmp);
      return (1);
    }
  else
    printf("History : %s : Unknow option\n", tmp[1]);
  my_free_wordtab(tmp);
  return (1);
}
