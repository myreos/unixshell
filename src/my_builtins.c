/*
** my_builtins.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Wed May  6 15:29:00 2015 grandr_r
** Last update Sun May 24 09:34:49 2015 grandr_r
*/

#include <stdlib.h>
#include <stdio.h>
#include <my.h>

int	my_builtins(t_42 **cmd, t_sh *sh, char *str, char **tmp)
{
  if (match(str, "cd*") == 1)
    return (my_cd(cmd[0], sh, str));
  else if (match(str, "echo*") == 1)
    return (my_echo(cmd[0], sh, str));
  else if (match(str, "exit*") == 1)
    return (my_exit(cmd[0], sh, str));
  else if (match(str, "env") == 1)
    return (my_env_print(sh->my_env));
  else if (match(str, "export*") == 1)
    return (my_export(str, sh));
  else if (match(str, "unsetenv*") == 1)
    return (my_unsetenv(str, sh));
  else if (match(str, "history*") == 1)
    return (my_history(str, sh, cmd));
  else if (match(str, "!*") == 1)
    return (hist_mark(str, sh, cmd));
  else if (match(str, "setenv*") == 1)
    return (my_setenv(str, sh));
  return (0);
}
