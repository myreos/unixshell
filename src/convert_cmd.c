/*
** convert_cmd.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Mon May 18 15:50:57 2015 grandr_r
** Last update Sun May 24 13:20:03 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <my.h>

int	char_is_alpha(char c)
{
  if ((c >= 'A' && c <= 'Z') || c == '?' || c == '_' || (c >= 'a' && c <= 'z'))
    return (1);
  return (0);
}

char	*get_var_name(char *s)
{
  char	*r;
  int	i;

  i = 0;
  if ((r = malloc(sizeof(char) * (my_strlen(s) + 1))) == NULL)
    exit(-1);
  while (char_is_alpha(s[i]) == 1 && s[i])
    {
      r[i] = s[i];
      i++;
    }
  r[i] = '\0';
  r = my_realloc(r, "=");
  return (r);
}

int	get_size(char *s, t_sh *sh)
{
  int	i;
  int	size;
  char	*s2;

  i = 0;
  size = 0;
  while (s[i])
    {
      if (s[i] == '$' && s[i + 1] != '\0')
	{
	  s2 = get_var_name(&s[++i]);
	  i += my_strlen(s2) - 1;
	  if (my_getenv(sh->my_env, s2) != NULL)
	    size += my_strlen(my_getenv(sh->my_env, s2));
	  free(s2);
	}
      else
	{
	  i++;
	  size++;
	}
    }
  return (size + 1);
}

char	*convert_cmd2(char *s)
{
  int	i;
  char	*ret;
  char	**wt;

  i = 0;
  ret = my_strdup(s);
  while (ret[i])
    {
      if (ret[i] == '|' && i != 0 && i != my_strlen(s))
	{
	  if (ret[i - 1] != '|' && ret[i + 1] != '|')
	    ret[i] = '{';
	}
      else if (ret[i] == '|' && (i == 0 || i == my_strlen(s)))
	ret[i] = '{';
      i++;
    }
  free(s);
  wt = my_str_to_wordtab(ret, 0, ' ');
  add_ls_alias(wt);
  free(ret);
  ret = my_unwordtab(wt);
  my_free_wordtab(wt);
  return (ret);
}

char	*convert_cmd(char *s, t_sh *sh, int i, int j)
{
  char	*r;
  char	*s2;

  if ((r = malloc(sizeof(char) * (get_size(s, sh)))) == NULL)
    exit(-1);
  while (s[i])
    {
      if (s[i] == '$' && s[i + 1] != '\0')
	{
	  s2 = get_var_name(&s[++i]);
	  i += my_strlen(s2) - 1;
	  if (my_getenv(sh->my_env, s2) != NULL)
	    {
	      r[j] = '\0';
	      r = my_strcat3(r, my_getenv(sh->my_env, s2));
	      j += my_strlen(my_getenv(sh->my_env, s2));
	    }
	  free(s2);
	}
      else
	r[j++] = s[i++];
    }
  r[j] = '\0';
  free(s);
  return (convert_cmd2(r));
}
