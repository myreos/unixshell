/*
** canon.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Mon Apr 13 18:51:35 2015 remy grandry
** Last update Sun May 24 20:58:39 2015 grandr_r
*/

#include <curses.h>
#include <termios.h>
#include <unistd.h>
#include <termcap.h>
#include <stdlib.h>
#include <my.h>

int	char_is_printable(char c)
{
  if (c >= 32 && c <= 126)
    return (1);
  else
    return (0);
}

int			canon_on(t_sh *sh)
{
  struct termios	attr;

  if (sh->termcap == 0)
    return (-1);
  if (tcgetattr(0, &attr) == -1)
    return (-1);
  attr.c_lflag &= ~(ICANON);
  attr.c_lflag &= ~ECHO;
  attr.c_cc[VTIME] = 0;
  attr.c_cc[VMIN] = 1;
  if (tcsetattr(0, TCSADRAIN, &attr) == -1)
    return (-1);
  return (0);
}

int			canon_off(t_sh *sh)
{
  struct termios	attr;

  if (sh->termcap == 0)
    return (-1);
  if (tcgetattr(0, &attr) == -1)
    return (-1);
  attr.c_lflag |= ICANON;
  attr.c_lflag |= ECHO;
  attr.c_cc[VTIME] = 0;
  attr.c_cc[VMIN] = 1;
  if (tcsetattr(0, TCSADRAIN, &attr) == -1)
    return (-1);
  return (0);
}
