/*
** my_free.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Wed May  6 15:36:11 2015 grandr_r
** Last update Sat May 23 09:59:16 2015 grandr_r
*/

#include <stdlib.h>
#include <my.h>

void	my_free_wordtab(char **wt)
{
  int	i;

  i = 0;
  if (wt == NULL)
    return;
  else if (wt[0] == NULL)
    {
      free(wt);
      return;
    }
  while (wt[i])
    {
      free(wt[i]);
      i++;
    }
  free(wt);
  wt = NULL;
}

void	my_free_wordtab2(char ***wt)
{
  int	i;

  i = 0;
  if (wt == NULL || wt[0] == NULL)
    return;
  while (wt[i])
    {
      my_free_wordtab(wt[i]);
      i++;
    }
  free(wt);
  wt = NULL;
}

void	my_free_wordtab3(char **wt, char **wt2)
{
  my_free_wordtab(wt);
  my_free_wordtab(wt2);
}
