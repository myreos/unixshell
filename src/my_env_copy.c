/*
** my_env_copy.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Fri May  8 19:09:43 2015 grandr_r
** Last update Wed May 13 20:17:02 2015 grandr_r
*/

#include <stdlib.h>
#include <my.h>

int	my_arrlen(char **arr)
{
  int	i;

  i = 0;
  if (arr == NULL || arr[0] == NULL)
    return (0);
  while (arr[i])
    i++;
  return (i);
}

int	my_env_print(char **env)
{
  int	i;

  i = 0;
  if (env == NULL)
    return (1);
  while (env[i])
    {
      if (env[i] != NULL)
	{
	  my_putstr(env[i], 1);
	  my_putchar('\n', 1);
	}
      i++;
    }
  return (1);
}

int	my_env_copy(t_sh *sh, char **env)
{
  int	i;

  i = 0;
  if (env[0] == NULL)
    return (-1);
  if ((sh->my_env = malloc(sizeof(char *) * (my_arrlen(env) + 1))) == NULL)
    exit(-1);
  while (env[i])
    {
      sh->my_env[i] = NULL;
      sh->my_env[i] = my_strdup(env[i]);
      i++;
    }
  sh->my_env[i] = NULL;
  return (0);
}
