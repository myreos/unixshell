/*
** myls.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Sun May 24 12:38:47 2015 grandr_r
** Last update Wed Jun  3 10:16:31 2015 grandr_r
*/

#include <stdlib.h>
#include <my.h>

int	is_neg(char *str)
{
  int	i;
  int	c;

  i = 0;
  c = 0;
  while ((str[i] > '9' || str[i] < '0') && str[i])
    {
      if (str[i] == '-')
        c++;
      i++;
    }
  if (c % 2 != 0)
    return (1);
  else
    return (0);
}

int             is_num(char l)
{
  if (l >= '0' && l <= '9')
    return (1);
  else
    return (0);
}

int	my_getnbr(char *str)
{
  int	i;
  int	nbr;

  nbr = 0;
  i = 0;
  if (str == NULL)
    return (0);
  while (is_num(str[i]) == 0 && str[i])
    i++;
  while (is_num(str[i]) == 1)
    {
      nbr = ((nbr * 10) + (str[i] - 48));
      i++;
    }
  if (is_neg(str) == 1)
    nbr = -nbr;
  return (nbr);
}

void	add_ls_alias(char **wt)
{
  int	i;

  i = 0;
  if (wt == NULL || wt[0] == NULL)
    return;
  while (wt[i])
    {
      if (match(wt[i], "ls") == 1)
	{
	  free(wt[i]);
	  wt[i] = my_strdup("ls --color=tty");
	}
      else if (match(wt[i], "grep") == 1)
	{
	  free(wt[i]);
	  wt[i] = my_strdup("grep --color=auto");
	}
      else
	i++;
    }
}
