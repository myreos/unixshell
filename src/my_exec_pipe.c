/*
** my_shell_exec.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Fri May  8 19:30:44 2015 grandr_r
** Last update Sun May 24 09:48:47 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <my.h>

char	*my_unwordtab(char **wt)
{
  int	i;
  char	*s;

  i = 1;
  s = my_strdup(wt[0]);
  while (wt[i])
    {
      s = my_realloc(s, " ");
      s = my_realloc(s, wt[i]);
      i++;
    }
  return (s);
}

int	shexec_pipe(char **tmp, char **env, t_sh *sh)
{
  if (execve(tmp[0], tmp, env) == -1)
    exec_error(tmp[0]);
  return (0);
}

void	my_logical_pipe_rec(t_sh *sh, char *tmp)
{
  char	**wt;

  if (pipe_is_redir(tmp, sh) == 1)
    return;
  else
    {
      wt = my_str_to_wordtab(tmp, 0, ' ');
      shexec_pipe(wt, sh->my_env, sh);
      my_free_wordtab(wt);
    }
  free(tmp);
}

void	my_logical_pipe(t_42 **cmd, t_sh *sh, char *tmp)
{
  char	**wt;

  if (pipe_is_redir(tmp, sh) == 1)
    return;
  else
    {
      wt = my_str_to_wordtab(tmp, 0, ' ');
      shexec_pipe(wt, sh->my_env, sh);
      my_free_wordtab(wt);
    }
}
