/*
** my_string2.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Sat May  9 11:46:12 2015 grandr_r
** Last update Sat May  9 11:57:53 2015 grandr_r
*/

#include <string.h>
#include <stdlib.h>
#include <my.h>

int	my_strncmp(char *s1, char *s2, int n)
{
  int   i;

  i = 0;
  if (s1 == NULL || s2 == NULL)
    return (-1);
  while ((s1[i] || s2[i]) && i < n)
    {
      if (s1[i] < s2[i])
        return (-1);
      if (s1[i] > s2[i])
        return (1);
      i = i + 1;
    }
  if ((s1[i] < s2[i]) && i < n)
    return (-1);
  if ((s1[i] > s2[i]) && i < n)
    return (1);
  return (0);
}
