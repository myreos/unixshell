/*
** stw.h for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Wed Mar 11 11:48:54 2015 remy grandry
** Last update Fri May 22 15:20:45 2015 grandr_r
*/

#ifndef STW_H_
# define STW_H_
#include "my.h"

# define BUFF_SIZE (100)

typedef struct	s_stw
{
  int		i;
  int		j;
  int		len;
}		t_stw;

typedef struct  s_tmp
{
  int           pres;
  char          *res;
}               t_tmp;

#endif /* !STW_H_ */
